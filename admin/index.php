<?php
session_start();
if (!isset($_SESSION['userid']))
{
    die('Bitte zuerst <a href="login.php">einloggen</a>');
}

//Abfrage der Nutzer ID vom Login
$userid = $_SESSION['userid'];
include ("../assets/html/header.php");
echo ('<div class="row">');
//Checks for and reads JSON files
if (file_exists('../waograbber/config.json'))
{
    $configFile = file_get_contents('../waograbber/config.json');
    if (file_exists('../waograbber/stations.json'))
    {
        $stationsFile = file_get_contents('../waograbber/stations.json');

        //request a List of now playing Tracks
        $nowPlayingURL = file_get_contents("https://api.tb-group.fm//v1/radio");

        //Serialize json files
        $config = json_decode($configFile, true);
        $stations = json_decode($stationsFile, true);
        $nowPlaying = json_decode($nowPlayingURL, true);

        include ("../waograbber/update.php");
        echo ('<div class="container">
        <div class="row justify-content-center p-3">');
        //This reads the Showplans
        foreach ($stations as $station)
        {
            if ($config['allowedStations'][$station['textId']] == true)
            {
                $apiReqUrl = 'https://api.tb-group.fm//v1/showplan/' . $station['numId'] . '/1';
                $apiRequest = file_get_contents($apiReqUrl);
                $showplan = json_decode($apiRequest, true);
                foreach ($showplan as $show)
                {
                    // Make UNIX Timestamp from API readable (for Human)
                    // But first fix the Timestamp
                    $unixStart = substr($show['s'], 0, -3);
                    $unixEnd = substr($show['e'], 0, -3);
                    $unixNow = time();
                    $start = date("d.m.Y H:i", $unixStart);
                    $end = date("H:i", $unixEnd);

                    //builds a Bootstrap Card Element for an Show on Air! (Noice!!!!)
                    if ($unixNow >= $unixStart and $unixNow <= $unixEnd)
                    {
                        echo '<div class="card" style="width: 28rem;">';
                        if ($show['m'] == "BitPitcher")
                        {
                            echo '<div class="card-header"><blink>🔴</blink><span class="badge text-bg-danger">ON AIR</span><span class="badge text-bg-secondary">Genre: ' . $show['ss'] . '</span><span class="badge text-bg-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-snapchat" viewBox="0 0 16 16">
                            <path d="M15.943 11.526c-.111-.303-.323-.465-.564-.599a1.416 1.416 0 0 0-.123-.064l-.219-.111c-.752-.399-1.339-.902-1.746-1.498a3.387 3.387 0 0 1-.3-.531c-.034-.1-.032-.156-.008-.207a.338.338 0 0 1 .097-.1c.129-.086.262-.173.352-.231.162-.104.289-.187.371-.245.309-.216.525-.446.66-.702a1.397 1.397 0 0 0 .069-1.16c-.205-.538-.713-.872-1.329-.872a1.829 1.829 0 0 0-.487.065c.006-.368-.002-.757-.035-1.139-.116-1.344-.587-2.048-1.077-2.61a4.294 4.294 0 0 0-1.095-.881C9.764.216 8.92 0 7.999 0c-.92 0-1.76.216-2.505.641-.412.232-.782.53-1.097.883-.49.562-.96 1.267-1.077 2.61-.033.382-.04.772-.036 1.138a1.83 1.83 0 0 0-.487-.065c-.615 0-1.124.335-1.328.873a1.398 1.398 0 0 0 .067 1.161c.136.256.352.486.66.701.082.058.21.14.371.246l.339.221a.38.38 0 0 1 .109.11c.026.053.027.11-.012.217a3.363 3.363 0 0 1-.295.52c-.398.583-.968 1.077-1.696 1.472-.385.204-.786.34-.955.8-.128.348-.044.743.28 1.075.119.125.257.23.409.31a4.43 4.43 0 0 0 1 .4.66.66 0 0 1 .202.09c.118.104.102.26.259.488.079.118.18.22.296.3.33.229.701.243 1.095.258.355.014.758.03 1.217.18.19.064.389.186.618.328.55.338 1.305.802 2.566.802 1.262 0 2.02-.466 2.576-.806.227-.14.424-.26.609-.321.46-.152.863-.168 1.218-.181.393-.015.764-.03 1.095-.258a1.14 1.14 0 0 0 .336-.368c.114-.192.11-.327.217-.42a.625.625 0 0 1 .19-.087 4.446 4.446 0 0 0 1.014-.404c.16-.087.306-.2.429-.336l.004-.005c.304-.325.38-.709.256-1.047Zm-1.121.602c-.684.378-1.139.337-1.493.565-.3.193-.122.61-.34.76-.269.186-1.061-.012-2.085.326-.845.279-1.384 1.082-2.903 1.082-1.519 0-2.045-.801-2.904-1.084-1.022-.338-1.816-.14-2.084-.325-.218-.15-.041-.568-.341-.761-.354-.228-.809-.187-1.492-.563-.436-.24-.189-.39-.044-.46 2.478-1.199 2.873-3.05 2.89-3.188.022-.166.045-.297-.138-.466-.177-.164-.962-.65-1.18-.802-.36-.252-.52-.503-.402-.812.082-.214.281-.295.49-.295a.93.93 0 0 1 .197.022c.396.086.78.285 1.002.338.027.007.054.01.082.011.118 0 .16-.06.152-.195-.026-.433-.087-1.277-.019-2.066.094-1.084.444-1.622.859-2.097.2-.229 1.137-1.22 2.93-1.22 1.792 0 2.732.987 2.931 1.215.416.475.766 1.013.859 2.098.068.788.009 1.632-.019 2.065-.01.142.034.195.152.195a.35.35 0 0 0 .082-.01c.222-.054.607-.253 1.002-.338a.912.912 0 0 1 .197-.023c.21 0 .409.082.49.295.117.309-.04.56-.401.812-.218.152-1.003.638-1.18.802-.184.169-.16.3-.139.466.018.14.413 1.991 2.89 3.189.147.073.394.222-.041.464Z"/>
                          </svg></span></div></div>';
                        }
                        else
                        {
                            echo '<div class="card-header"><blink>🔴</blink><span class="badge text-bg-danger">ON AIR</span><span class="badge text-bg-secondary">Genre: ' . $show['ss'] . '</span></div>';
                        }
                        echo '<img class="card-img-top" src="' . $station['logourl'] . '" alt="' . $station['name'] . ' Logo">';
                        echo '<div class="card-body">';
                        echo '<h5 class="card-title">🎧' . $show['m'] . ' <br> 🎙️' . $show['n'] . '</h5>';
                        echo '<p class="card-text"> 🕒 ' . $start . ' bis ' . $end . '<br><br><marquee>🎵' . $nowPlaying[$station['textId']]['a'] . ' - ' . $nowPlaying[$station['textId']]['t'] . ' - 📻' . $station['name'] . ' - We aRe oNe</marquee></p><a class="btn btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="' . $station['name'] . ' aufrufen" href=' . $station['stream'] . ' role="button"> 📻 ' . $station['name'] . '</a></div>';
                        // Calculate how long the Show is still running
                        $secondsRemain = $unixEnd - $unixNow;
                        $minutesRemain = $secondsRemain / 60;
                        $minutesRemain = round($minutesRemain);
                        $hoursRemain = $minutesRemain / 60;
                        $hoursRemain = round($hoursRemain);
                        if ($minutesRemain < 1)
                        {
                            echo '<div class="card-footer text-muted">Noch ' . $secondsRemain . ' Sekunden</div></div></div>';
                        }
                        elseif ($minutesRemain <= 60)
                        {
                            echo '<div class="card-footer text-muted">Noch ' . $minutesRemain . ' Minuten</div></div></div>';
                        }
                        elseif ($hoursRemain == 1)
                        {
                            echo '<div class="card-footer text-muted">Noch eine Stunde</div></div></div>';
                        }
                        else
                        {
                            echo '<div class="card-footer text-muted">Noch ' . $hoursRemain . ' Stunden</div></div></div>';
                        }
                    }
                }
            }
        }
        echo '<p>
                        <a class="btn btn-primary" data-bs-toggle="collapse" href="#upcomingShows" role="button" aria-expanded="false" aria-controls="upcomingShows">Beendete Sendungen</a>
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#recentShows" aria-expanded="false" aria-controls="recentShows">Kommende Sendungen</button>
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target=".multi-collapse" aria-expanded="false" aria-controls="upcomingShows recentShows">Zeige alle Tabellen</button>
                    </p>
                        <div class="row">
                            <div class="col">
                                <div class="collapse multi-collapse" id="upcomingShows">
                                    <div class="card card-body">';
        foreach ($stations as $station)
        {
            if ($config['allowedStations'][$station['textId']] == true)
            {
                $apiReqUrl = 'https://api.tb-group.fm//v1/showplan/' . $station['numId'] . '/1';
                $apiRequest = file_get_contents($apiReqUrl);
                $showplan = json_decode($apiRequest, true);
                foreach ($showplan as $show)
                {
                    // Make UNIX Timestamp from API readable (for Human)
                    // But first fix the Timestamp
                    $unixStart = substr($show['s'], 0, -3);
                    $unixEnd = substr($show['e'], 0, -3);
                    $unixNow = time();
                    $start = date("d.m.Y H:i", $unixStart);
                    $end = date("H:i", $unixEnd);
                    echo '<ul class="list-group list-group-flush">';
                    if ($unixNow > $unixStart and $unixNow > $unixEnd)
                    {
                        if ($show['m'] == "BitPitcher")
                        {
                            echo '<li class="list-group-item"><span class="badge text-bg-secondary">Beendete Sendung</span><span class="badge text-bg-secondary">Genre: ' . $show['ss'] . '</span><span class="badge text-bg-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-snapchat" viewBox="0 0 16 16">
                                                        <path d="M15.943 11.526c-.111-.303-.323-.465-.564-.599a1.416 1.416 0 0 0-.123-.064l-.219-.111c-.752-.399-1.339-.902-1.746-1.498a3.387 3.387 0 0 1-.3-.531c-.034-.1-.032-.156-.008-.207a.338.338 0 0 1 .097-.1c.129-.086.262-.173.352-.231.162-.104.289-.187.371-.245.309-.216.525-.446.66-.702a1.397 1.397 0 0 0 .069-1.16c-.205-.538-.713-.872-1.329-.872a1.829 1.829 0 0 0-.487.065c.006-.368-.002-.757-.035-1.139-.116-1.344-.587-2.048-1.077-2.61a4.294 4.294 0 0 0-1.095-.881C9.764.216 8.92 0 7.999 0c-.92 0-1.76.216-2.505.641-.412.232-.782.53-1.097.883-.49.562-.96 1.267-1.077 2.61-.033.382-.04.772-.036 1.138a1.83 1.83 0 0 0-.487-.065c-.615 0-1.124.335-1.328.873a1.398 1.398 0 0 0 .067 1.161c.136.256.352.486.66.701.082.058.21.14.371.246l.339.221a.38.38 0 0 1 .109.11c.026.053.027.11-.012.217a3.363 3.363 0 0 1-.295.52c-.398.583-.968 1.077-1.696 1.472-.385.204-.786.34-.955.8-.128.348-.044.743.28 1.075.119.125.257.23.409.31a4.43 4.43 0 0 0 1 .4.66.66 0 0 1 .202.09c.118.104.102.26.259.488.079.118.18.22.296.3.33.229.701.243 1.095.258.355.014.758.03 1.217.18.19.064.389.186.618.328.55.338 1.305.802 2.566.802 1.262 0 2.02-.466 2.576-.806.227-.14.424-.26.609-.321.46-.152.863-.168 1.218-.181.393-.015.764-.03 1.095-.258a1.14 1.14 0 0 0 .336-.368c.114-.192.11-.327.217-.42a.625.625 0 0 1 .19-.087 4.446 4.446 0 0 0 1.014-.404c.16-.087.306-.2.429-.336l.004-.005c.304-.325.38-.709.256-1.047Zm-1.121.602c-.684.378-1.139.337-1.493.565-.3.193-.122.61-.34.76-.269.186-1.061-.012-2.085.326-.845.279-1.384 1.082-2.903 1.082-1.519 0-2.045-.801-2.904-1.084-1.022-.338-1.816-.14-2.084-.325-.218-.15-.041-.568-.341-.761-.354-.228-.809-.187-1.492-.563-.436-.24-.189-.39-.044-.46 2.478-1.199 2.873-3.05 2.89-3.188.022-.166.045-.297-.138-.466-.177-.164-.962-.65-1.18-.802-.36-.252-.52-.503-.402-.812.082-.214.281-.295.49-.295a.93.93 0 0 1 .197.022c.396.086.78.285 1.002.338.027.007.054.01.082.011.118 0 .16-.06.152-.195-.026-.433-.087-1.277-.019-2.066.094-1.084.444-1.622.859-2.097.2-.229 1.137-1.22 2.93-1.22 1.792 0 2.732.987 2.931 1.215.416.475.766 1.013.859 2.098.068.788.009 1.632-.019 2.065-.01.142.034.195.152.195a.35.35 0 0 0 .082-.01c.222-.054.607-.253 1.002-.338a.912.912 0 0 1 .197-.023c.21 0 .409.082.49.295.117.309-.04.56-.401.812-.218.152-1.003.638-1.18.802-.184.169-.16.3-.139.466.018.14.413 1.991 2.89 3.189.147.073.394.222-.041.464Z"/>
                                                      </svg></span><span class="badge text-bg-light">' . $station['name'] . '</span> - ' . $show['m'] . ' - ' . $show['n'] . '</li>';
                        }
                        else
                        {
                            echo '<li class="list-group-item"><span class="badge text-bg-secondary">Beendete Sendung</span><span class="badge text-bg-secondary">Genre: ' . $show['ss'] . '</span><span class="badge text-bg-light">' . $station['name'] . '</span> - ' . $show['m'] . ' - ' . $show['n'] . '</li>';
                        }
                    }
                }
            }
        }
        echo '</ul></div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="collapse multi-collapse" id="recentShows">
                                    <div class="card card-body">';
        foreach ($stations as $station)
        {
            if ($config['allowedStations'][$station['textId']] == true)
            {
                $apiReqUrl = 'https://api.tb-group.fm//v1/showplan/' . $station['numId'] . '/1';
                $apiRequest = file_get_contents($apiReqUrl);
                $showplan = json_decode($apiRequest, true);
                foreach ($showplan as $show)
                {
                    // Make UNIX Timestamp from API readable (for Human)
                    // But first fix the Timestamp
                    $unixStart = substr($show['s'], 0, -3);
                    $unixEnd = substr($show['e'], 0, -3);
                    $unixNow = time();
                    $start = date("d.m.Y H:i", $unixStart);
                    $end = date("H:i", $unixEnd);
                    echo '<ul class="list-group list-group-flush">';
                    if ($unixNow < $unixStart)
                    {
                        if ($show['m'] == "BitPitcher")
                        {
                            echo '<li class="list-group-item"><span class="badge text-bg-secondary">Anstehende Sendung</span><span class="badge text-bg-secondary">Genre: ' . $show['ss'] . '</span><span class="badge text-bg-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-snapchat" viewBox="0 0 16 16">
                                                        <path d="M15.943 11.526c-.111-.303-.323-.465-.564-.599a1.416 1.416 0 0 0-.123-.064l-.219-.111c-.752-.399-1.339-.902-1.746-1.498a3.387 3.387 0 0 1-.3-.531c-.034-.1-.032-.156-.008-.207a.338.338 0 0 1 .097-.1c.129-.086.262-.173.352-.231.162-.104.289-.187.371-.245.309-.216.525-.446.66-.702a1.397 1.397 0 0 0 .069-1.16c-.205-.538-.713-.872-1.329-.872a1.829 1.829 0 0 0-.487.065c.006-.368-.002-.757-.035-1.139-.116-1.344-.587-2.048-1.077-2.61a4.294 4.294 0 0 0-1.095-.881C9.764.216 8.92 0 7.999 0c-.92 0-1.76.216-2.505.641-.412.232-.782.53-1.097.883-.49.562-.96 1.267-1.077 2.61-.033.382-.04.772-.036 1.138a1.83 1.83 0 0 0-.487-.065c-.615 0-1.124.335-1.328.873a1.398 1.398 0 0 0 .067 1.161c.136.256.352.486.66.701.082.058.21.14.371.246l.339.221a.38.38 0 0 1 .109.11c.026.053.027.11-.012.217a3.363 3.363 0 0 1-.295.52c-.398.583-.968 1.077-1.696 1.472-.385.204-.786.34-.955.8-.128.348-.044.743.28 1.075.119.125.257.23.409.31a4.43 4.43 0 0 0 1 .4.66.66 0 0 1 .202.09c.118.104.102.26.259.488.079.118.18.22.296.3.33.229.701.243 1.095.258.355.014.758.03 1.217.18.19.064.389.186.618.328.55.338 1.305.802 2.566.802 1.262 0 2.02-.466 2.576-.806.227-.14.424-.26.609-.321.46-.152.863-.168 1.218-.181.393-.015.764-.03 1.095-.258a1.14 1.14 0 0 0 .336-.368c.114-.192.11-.327.217-.42a.625.625 0 0 1 .19-.087 4.446 4.446 0 0 0 1.014-.404c.16-.087.306-.2.429-.336l.004-.005c.304-.325.38-.709.256-1.047Zm-1.121.602c-.684.378-1.139.337-1.493.565-.3.193-.122.61-.34.76-.269.186-1.061-.012-2.085.326-.845.279-1.384 1.082-2.903 1.082-1.519 0-2.045-.801-2.904-1.084-1.022-.338-1.816-.14-2.084-.325-.218-.15-.041-.568-.341-.761-.354-.228-.809-.187-1.492-.563-.436-.24-.189-.39-.044-.46 2.478-1.199 2.873-3.05 2.89-3.188.022-.166.045-.297-.138-.466-.177-.164-.962-.65-1.18-.802-.36-.252-.52-.503-.402-.812.082-.214.281-.295.49-.295a.93.93 0 0 1 .197.022c.396.086.78.285 1.002.338.027.007.054.01.082.011.118 0 .16-.06.152-.195-.026-.433-.087-1.277-.019-2.066.094-1.084.444-1.622.859-2.097.2-.229 1.137-1.22 2.93-1.22 1.792 0 2.732.987 2.931 1.215.416.475.766 1.013.859 2.098.068.788.009 1.632-.019 2.065-.01.142.034.195.152.195a.35.35 0 0 0 .082-.01c.222-.054.607-.253 1.002-.338a.912.912 0 0 1 .197-.023c.21 0 .409.082.49.295.117.309-.04.56-.401.812-.218.152-1.003.638-1.18.802-.184.169-.16.3-.139.466.018.14.413 1.991 2.89 3.189.147.073.394.222-.041.464Z"/>
                                                      </svg></span><span class="badge text-bg-light">' . $station['name'] . '</span> - ' . $show['m'] . ' - ' . $show['n'] . '</li>';
                        }
                        else
                        {
                            echo '<li class="list-group-item"><span class="badge text-bg-secondary">Anstehende Sendung</span><span class="badge text-bg-secondary">Genre: ' . $show['ss'] . '</span><span class="badge text-bg-light">' . $station['name'] . '</span> - ' . $show['m'] . ' - ' . $show['n'] . '</li>';
                        }
                    }
                }
            }
        }
        echo '</ul>';
        echo '</div>
                                </div>
                            </div>
                        </div>';
        if ($config['adminSettings']['tracklist'] == true)
        {
            echo ('<div class="container">
                                <div class="row justify-content-center p-3">');
            foreach ($stations as $station)
            {
                if ($config['allowedStations'][$station['textId']] == "true")
                {
                    echo ('<div class="card" style="width: 32rem;"> <div class="card-header">');
                    $apiReqUrl = 'https://api.tb-group.fm//v1/tracklist/' . $station['numId'] . '?count=30';
                    $apiRequest = file_get_contents($apiReqUrl);
                    $tracklist = json_decode($apiRequest, true);

                    echo ($station['name'] . '</div><ul class="list-group list-group-flush">');
                    foreach ($tracklist as $track)
                    {
                        $unixPlayed = substr($track['p'], 0, -3);
                        $played = date("H:i", $unixPlayed);
                        if ($track['u'] !== "WeaReoNe")
                        {
                            echo '<li class="list-group-item"><span class="badge text-bg-danger"> ' . $track['u'] . '</span> - ' . $track['a'] . ' - ' . $track['t'] . '<br><br> Gespielt um ' . $played . ' Uhr</li>';
                        }
                        else
                        {
                            echo '<li class="list-group-item"><span class="badge text-bg-light">AutoDJ</span> - ' . $track['a'] . ' - ' . $track['t'] . ' <br><br> Gespielt um ' . $played . ' Uhr</li>';
                        }
                    }
                    echo ('</ul></div><br><br>');
                }
            }
        }

        echo '</div>';
        // Elses to Check Missing File Error
        
    }
    else
    {
        echo '<div class="alert alert-danger" role="alert"> stations.json konnte nicht gefunden werden. (Das ist nicht gut!!)</div>';
    }
}
else
{
    echo '<div class="alert alert-danger" role="alert"> config.json konnte nicht gefunden werden. (Das ist nicht gut!!)</div>';
}
echo '<a class="btn btn-danger" href="../admin/logout.php" role="button">Abmelden</a></div>';
include ("../assets/html/footer.php");
?>
