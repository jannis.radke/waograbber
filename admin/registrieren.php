<?php
$configFile = file_get_contents('../waograbber/config.json');
$dbjson = file_get_contents('configs/dbconf.json');
$config = json_decode($configFile, true);
$db = json_decode($dbjson, true);
session_start();
include ("../assets/html/header.php");
$pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['database'], $db['user'], $db['password']);
?>
<!DOCTYPE html> 
<html> 
<head>
  <title>Registrierung</title>    
</head> 
<body>
 
<?php
$showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll
if ($config['adminSettings']['register'] == true)
{
    if (isset($_GET['register']))
    {
        $error = false;
        $email = $_POST['email'];
        $passwort = $_POST['passwort'];
        $passwort2 = $_POST['passwort2'];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            echo '<div class="alert alert-danger" role="alert">Bitte geben Sie eine Gültige E-Mail Adresse an</div><br>';
            $error = true;
        }
        if (strlen($passwort) == 0)
        {
            echo '<div class="alert alert-danger" role="alert">Bitte geben Sie ein Passwort an!</div><br>';
            $error = true;
        }
        if ($passwort != $passwort2)
        {
            echo '<div class="alert alert-danger" role="alert">Die Passwörter müssen übereinstimmen!</div><br>';
            $error = true;
        }

        //Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
        if (!$error)
        {
            $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
            $result = $statement->execute(array(
                'email' => $email
            ));
            $user = $statement->fetch();

            if ($user !== false)
            {
                echo '<div class="alert alert-danger" role="alert">Diese E-Mail Adresse ist bereits vergeben!</div><br>';
                $error = true;
            }
        }

        //Keine Fehler, wir können den Nutzer registrieren
        if (!$error)
        {
            $passwort_hash = password_hash($passwort, PASSWORD_DEFAULT);

            $statement = $pdo->prepare("INSERT INTO users (email, passwort) VALUES (:email, :passwort)");
            $result = $statement->execute(array(
                'email' => $email,
                'passwort' => $passwort_hash
            ));

            if ($result)
            {
                echo '<div class="alert alert-success" role="alert">Du wurdest erfolgreich registriert. <a href="login.php">Zum Login</a></div>';
                $showFormular = false;
            }
            else
            {
                echo '<div class="alert alert-danger" role="alert">Es ist ein Fehler aufgetreten!</div><br>';
            }
        }
    }

    if ($showFormular)
    {
?>
 
    <form action="?register=1" method="post">
    E-Mail:<br>
    <input type="email" size="40" maxlength="250" name="email"><br><br>
 
    Dein Passwort:<br>
    <input type="password" size="40"  maxlength="250" name="passwort"><br>
 
    Passwort wiederholen:<br>
    <input type="password" size="40" maxlength="250" name="passwort2"><br><br>
 
    <input type="submit" value="Abschicken">
    </form>
 
    <?php
    } //Ende von if($showFormular)
    
}
else
{
    echo '<div class="alert alert-danger" role="alert">Registrierung ist ausgeschaltet!</div><br>';
}
include ("../assets/html/footer.php")
?>
