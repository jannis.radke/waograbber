<?php
$configFile = file_get_contents('waograbber/config.json');
$config = json_decode($configFile, true);
if ($config['adminSettings']['maintenance'] == "true"){
  die("Diese Website ist zur Zeit nicht verfügbar!");
}
include ("assets/html/header.php");
?>
  <!-- ======= Mobile nav toggle button ======= -->
  <!-- <button type="button" class="mobile-nav-toggle d-xl-none"><i class="bi bi-list mobile-nav-toggle"></i></button> -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex flex-column justify-content-center">
  </header><!-- End Header -->
  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Über mich</h2>
          <p>mein Name ist Jannis (aka. Kuro/KuroNeko, Quro), ich bin 25 Jahre aus Rendsburg und so viel gibt es eigentlich garnicht über mich zu erzählen.</p>
        </div>

        <div class="row">
          <div class="col-lg-4">
            <img src="assets/img/profile.jpeg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-8 pt-4 pt-lg-0 content">
            <h3>Quro kurz und Knapp:</h3>
            <p class="fst-italic">
              Mit viel Humor und guter Laune stehe ich gerne mit Ihren wünschen als DJ auch auf Ihrer Veranstaltung zur Verfügung. Kontaktieren Sie mich einfach!
            </p>
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Geburtstag:</strong> <span>23 Juli 1997</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span><a href="https://djquro.net">https://djquro.net</a></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Wohnort:</strong> <span>Westerrönfeld, Deutschland</span></li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Alter:</strong> <span>25</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>E-Mail:</strong> <span>quro@djquro.net</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Vertretende Projekte:</strong> <span><a href="https://replay.fm">Replay.FM</a> - We aRe oNe</span></li>
                </ul>
              </div>
            </div>
            <p>
              Schon in meiner frühen Jugend habe wollte ich andere Leute Unterhalten. Zunächst in einer Theatergruppe, später alleine als DJ auf privaten Veranstaltungen. Dabei bin ich für (fast) alle Genres offen
              Finden Sie mich als Gaming-Content Creator auf Twitch oder in weiteren, hier verlinkten Projekten!
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Sendeplan</h2>
        <?php 
        include ("waograbber/sendeplan.php");
        ?>
</div>
  </main><!-- End #main -->

<?php include ("assets/html/footer.php");?>
