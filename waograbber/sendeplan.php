<?php
if (!isset($shows))
{
    $shows = 0;
}
//Checks for and reads JSON files
if (file_exists('waograbber/config.json'))
{
    $configFile = file_get_contents('waograbber/config.json');
    if (file_exists('waograbber/stations.json'))
    {
        $stationsFile = file_get_contents('waograbber/stations.json');

        //request a List of now playing Tracks
        $nowPlayingURL = file_get_contents("https://api.tb-group.fm//v1/radio");

        //Serialize json files
        $config = json_decode($configFile, true);
        $stations = json_decode($stationsFile, true);
        $nowPlaying = json_decode($nowPlayingURL, true);

        $maxDays = $config['showlist']['days'] - 1;

        echo '<p>Mein Sendeplan, ' . $maxDays . ' Tage im Voraus!</p></div>';

        //This reads the Showplans
        foreach ($stations as $station)
        {
            if ($config['allowedStations'][$station['textId']] == "true")
            {
                for ($i = 0;;$i++)
                {
                    if ($i > $config['showlist']['days'])
                    {
                        break;
                    }
                    $apiReqUrl = 'https://api.tb-group.fm//v1/showplan/' . $station['numId'] . '/' . $i;
                    $apiRequest = file_get_contents($apiReqUrl);
                    $showplan = json_decode($apiRequest, true);
                    foreach ($showplan as $show)
                    {
                        // Filters for Shows only from specified DJ
                        if ($show['m'] == $config['showlist']['dj-name'])
                        {
                            $shows++;

                            // Make UNIX Timestamp from API readable (for Human)
                            // But first fix the Timestamp
                            $unixStart = substr($show['s'], 0, -3);
                            $unixEnd = substr($show['e'], 0, -3);
                            $unixNow = time();
                            $start = date("d.m.Y H:i", $unixStart);
                            $end = date("H:i", $unixEnd);

                            //builds a Bootstrap Card Element for an Show on Air! (Noice!!!!)
                            if ($unixNow >= $unixStart and $unixNow <= $unixEnd)
                            {
                                echo '<div class="card" style="width: 28rem;">';
                                echo '<div class="card-header"><blink>🔴</blink><span class="badge text-bg-danger">ON AIR</span></div>';
                                echo '<img class="card-img-top" src="' . $station['logourl'] . '" alt="' . $station['name'] . ' Logo">';
                                echo '<div class="card-body">';
                                echo '<h5 class="card-title">🎧' . $show['m'] . ' <br> 🎙️' . $show['n'] . '</h5>';
                                echo '<p class="card-text"> 🕒 ' . $start . ' bis ' . $end . '<br><br><marquee>🎵' . $nowPlaying[$station['textId']]['a'] . ' - ' . $nowPlaying[$station['textId']]['t'] . ' - 📻' . $station['name'] . ' - We aRe oNe</marquee></p><a class="btn btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="' . $station['name'] . ' aufrufen" href=' . $station['stream'] . ' role="button"> 📻 ' . $station['name'] . '</a></div>';
                                // Calculate how long the Show is still running
                                $secondsRemain = $unixEnd - $unixNow;
                                $minutesRemain = $secondsRemain / 60;
                                $minutesRemain = round($minutesRemain);
                                $hoursRemain = $minutesRemain / 60;
                                $hoursRemain = round($hoursRemain);
                                if ($minutesRemain < 1)
                                {
                                    echo '<div class="card-footer text-muted">Noch ' . $secondsRemain . ' Sekunden</div></div><br><br>';
                                }
                                elseif ($minutesRemain <= 60)
                                {
                                    echo '<div class="card-footer text-muted">Noch ' . $minutesRemain . ' Minuten</div></div><br><br>';
                                }
                                elseif ($hoursRemain == 1)
                                {
                                    echo '<div class="card-footer text-muted">Noch eine Stunde</div></div><br><br>';
                                }
                                else
                                {
                                    echo '<div class="card-footer text-muted">Noch ' . $hoursRemain . ' Stunden</div></div><br><br>';
                                }
                                //builds a Bootstrap Card Element for fineshed shows - Only if allowed in config
                                
                            }
                            elseif ($i == 0 or $unixEnd <= $unixNow)
                            {
                                if ($config['adminSettings']['showPastShows'] == true)
                                {
                                    echo '<div class="card" style="width: 28rem; color:#B00B15">';
                                    echo '<div class="card-header"> Vergangene Sendung </div>';
                                    echo '<img class="card-img-top" src="' . $station['logourl'] . '" alt="' . $station['name'] . ' Logo">';
                                    echo '<div class="card-body">';
                                    echo '<h5 class="card-title">🎧' . $show['m'] . ' <br>🎙️' . $show['n'] . '</h5>';
                                    echo '<p class="card-text"><br>🕒' . $start . ' bis ' . $end . '</p></div>';
                                    // Calculate xxx min/hrs ago for finished Show
                                    $secondsAgo = $unixNow - $unixEnd;
                                    $minutesAgo = $secondsAgo / 60;
                                    $minutesAgo = round($minutesAgo);
                                    $hoursAgo = $minutesAgo / 60;
                                    $hoursAgo = round($hoursAgo);

                                    if ($minutesAgo < 1)
                                    {
                                        echo '<div class="card-footer text-muted">Vor ' . $secondsAgo . ' Sekunden</div></div><br><br>';
                                    }
                                    elseif ($minutesAgo <= 60)
                                    {
                                        echo '<div class="card-footer text-muted">Vor ' . $minutesAgo . ' Minuten</div></div><br><br>';
                                    }
                                    elseif ($hoursAgo == 1)
                                    {
                                        echo '<div class="card-footer text-muted">Vor einer Stunde</div></div><br><br>';
                                    }
                                    else
                                    {
                                        echo '<div class="card-footer text-muted">Vor ' . $hoursAgo . ' Stunden</div></div><br><br>';
                                    }
                                }
                                //builds a Bootstrap Card Element for upcoming shows...
                                
                            }
                            else
                            {
                                echo '<div class="card" style="width: 28rem;">';
                                echo '<img class="card-img-top" src="' . $station['logourl'] . '" alt="' . $station['name'] . ' Logo">';
                                echo '<div class="card-body">';
                                echo '<h5 class="card-title">🎧' . $show['m'] . ' <br>🎙️' . $show['n'] . '</h5>';
                                echo '<p class="card-text"><br>🕒' . $start . ' bis ' . $end . '</p><a class="btn btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="' . $station['name'] . ' aufrufen" href=' . $station['stream'] . ' role="button"> 📻 ' . $station['name'] . '</a></div>';
                                $secondsRemain = $unixStart - $unixNow;
                                $minutesRemain = $secondsRemain / 60;
                                $minutesRemain = round($minutesRemain);
                                $hoursRemain = $minutesRemain / 60;
                                $hoursRemain = round($hoursRemain);
                                $daysRemain = $hoursRemain / 24;
                                $daysRemain = round($daysRemain);
                                if ($minutesRemain < 1)
                                {
                                    echo '<div class="card-footer text-muted">In ' . $secondsRemain . ' Sekunden</div></div><br><br>';
                                }
                                elseif ($minutesRemain <= 60)
                                {
                                    echo '<div class="card-footer text-muted">In ' . $minutesRemain . ' Minuten</div></div><br><br>';
                                }
                                elseif ($hoursRemain == 1)
                                {
                                    echo '<div class="card-footer text-muted">In einer Stunde</div></div><br><br>';
                                }
                                elseif ($daysRemain < 1)
                                {
                                    echo '<div class="card-footer text-muted">In ' . $hoursRemain . ' Stunden</div></div><br><br>';
                                }
                                else
                                {
                                    echo '<div class="card-footer text-muted">In ' . $daysRemain . ' Tagen </div></div><br><br>';
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($config['telegram']['enableChannel'] == true)
        {
            echo '<a class="btn btn-outline-primary" href="' . $config['telegram']['channelLink'] . '"><i class="bi bi-telegram"></i> Telegramchannel</a>';
        }
        if ($config['telegram']['enableBot'] == true)
        {
            echo '<a class="btn btn-outline-primary"href="' . $config['telegram']['botLink'] . '"><i class="bi bi-telegram"></i> Telegrambot</a>';
        }
        // Shows a sad message if DJ has no Shows entered
        if (!isset($shows))
        {
            $shows = 0;
        }
        if ($shows == 0)
        {
            if ($config['telegram']['enableChannel'] == true)
            {
                echo '<div class="alert alert-warning" role="alert">' . $config['showlist']['dj-name'] . ' hat zurzeit keine eingetragenen Shows!<br>
            Verpasse keine Show von ' . $config['showlist']['dj-name'] . ', klicke <a href="' . $config['telegram']['channelLink'] . '">hier</a> um den Telegramm Channel zu abonnieren!</div>';
            }
            else
            {
                echo '<div class="alert alert-warning" role="alert">' . $config['showlist']['dj-name'] . ' hat zurzeit keine eingetragenen Shows!</div>';
            }

        }
        // Elses to Check Missing File Error
        
    }
    else
    {
        echo '<div class="alert alert-danger" role="alert"> stations.json konnte nicht gefunden werden. (Das ist nicht gut!!)</div>';
    }
}
else
{
    echo '<div class="alert alert-danger" role="alert"> config.json konnte nicht gefunden werden. (Das ist nicht gut!!)</div>';
}
?>
