<?php
set_error_handler(
    function ($severity, $message, $file, $line) {
        throw new ErrorException($message, $severity, $severity, $file, $line);
    }
);

$localVersionJSON = file_get_contents("../waograbber/version.json");
$localVersion = json_decode($localVersionJSON, true);
  
try{
    $remoteVersionJSON = file_get_contents("https://gitlab.com/jannis.radke/waograbber/-/raw/" . $localVersion['branch'] . "/waograbber/version.json");
} catch(Exception $e){
    echo '<div class="alert alert-danger" role="alert">Waograbber kann keine Updateprüfung durchführen.<br><br>' . $e->getMessage() . '<br><br><b> Bitte versuchen Sie es später erneut!</b></b> </div>';
}
restore_error_handler();

if(!isset($e)){
    $remoteVersion = json_decode($remoteVersionJSON, true);
    if ($localVersion['branch'] !== 'main')
{
    echo '<div class="alert alert-danger" role="alert">Sie nutzen den <b>' . $localVersion['branch'] . '</b> Branch. Wir raten dringends von der produktiven Nutzung des Branches ab</div>';
}
        if ($localVersion['version'] !== $remoteVersion['version'])
    {
        echo '<div class="alert alert-warning" role="alert">Ihre Version von Waograbber ist veraltet! Ihre Version: <b>' . $localVersion['version'] . '</b> aktuelle Version: <b>' . $remoteVersion['version'] . '</b> </div>';
        echo '<button type="button" class="btn btn-warning btn-sm">Jetzt aktualisieren!</button>';
    }
}
